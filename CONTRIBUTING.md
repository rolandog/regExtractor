# Contribute

Here are some Command line instructions to help you out

## Git global setup

```
git config --global user.name "Your Name"
git config --global user.email "your.email@example.com"
```

## Create a new repository

```
git clone git@gitlab.com:rolandog/regExtractor.git
cd regExtractor
```

## Existing folder or Git repository

```
cd existing_folder
git init
git remote add origin git@gitlab.com:rolandog/regExtractor.git
```