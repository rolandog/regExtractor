/**
 * Returns an array of matches from a text.
 *
 * @param {text} input The cell containing text to parse.
 * @param {pattern} input The regular expression.
 * @param {flags} input (optional) Which regexp flags to use (e.g. 'gim', 'gm', 'g' ).
 * @return An array of matches.
 * @customfunction
 */
function regExtractor(text, pattern, flags) {
    "use strict";
    /* e.g ([0-9]{4,5})(?:,| ) *([0-9\.]{1,3})(?:,|\s)\s*([a-zA-Z ]*)
     * will have several matches for:
     *     11111 1 John
     *     2222 2,
     *     33333 30,
     *     5555, 0.5, Bob Loblaw
     *     4444 40
     *     1111 1 Some Guy
     *     3212 1 Amber
     *     5678, 10, Jerry
     * With the following trickery, we can determine how many
     * capturing groups there are, so we can place them in separate
     * columns
     */

    // match capturing groups
    var parenthRE = /\((?!\?\:).+?\)/gi;
    // how many capturing groups there are in the provided pattern
    var parenthCount = pattern.match(parenthRE).length;

    // RE from pattern
    var regExp = new RegExp(pattern, flags);

    // Get all matches
    var matches = text.match(regExp);

    // initialize
    var replacePattern = "";
    // iteration variable
    var i = 1;

    // iterate from i = 1 to i === length
    while (i <= parenthCount) {
        // e.g. replacePattern = "$1, $2, $3 ..."
        replacePattern += "$" + i;
        // as long as this isn't the last iteration
        if (i < parenthCount) {
            replacePattern += ", ";
        }
        i += 1;
    }

    /**
     * Transforms a matched text into an array (according to the
     * capturing groups of the Regular Expression)
     *
     * @param {match} input The string of a match.
     * @return An array of the captured groups in a match.
     */
    function replacer(match) {
        var replaced = match.replace(regExp, replacePattern);
        return replaced.split(", ");
    }

    // Get the matches, and split each match into columns
    if (replacePattern !== undefined) {
        return matches.map(replacer);
    } else {
        return matches;
    }
}
