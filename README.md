# regExtractor

This is a simple Google Apps Script that allows you to transform some
semi-formatted text (e.g user input in Google Forms) into separate rows and
columns.

## Example

Try to read the following Regular Expression.

```
([0-9]{4,5})(?:,| ) *([0-9\.]{1,3})(?:,|\s)\s*([a-zA-Z ]*)
```

Notice the prescence of Capturing Groups (which is a pattern enclosed in 
parenthesis).

These groups will help us separate each match into a column.

The previous Regular Expression will have several matches for:

> 11111 1 John
> 2222 2,
> 33333 30,
> 5555, 0.5, Bob Loblaw
> 4444 40
> 1111 1 Some Guy
> 3212 1 Amber
> 5678, 10, Jerry

By countng the expected matching groups, we can format the information into a
more manageable structure. For instance, the script will automatically generate
the following pattern which in turn will help us split the formatted data into
an array.

```
$1, $2, $3
```

This allows us to use the `split` method and convert the formatted text into an
array.

## Contribute

Here are some Command line instructions to help you out

### Git global setup

```
git config --global user.name "Your Name"
git config --global user.email "your.email@example.com"
```

### Create a new repository

```
git clone git@gitlab.com:rolandog/regExtractor.git
cd regExtractor
```

### Existing folder or Git repository

```
cd existing_folder
git init
git remote add origin git@gitlab.com:rolandog/regExtractor.git
```